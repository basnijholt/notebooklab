const Notebook = require('./src/index.vue');

module.exports = {
  install: function(_vue) {
    _vue.component('notebook-lab', Notebook);
  },
};
