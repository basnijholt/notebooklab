import Vue from 'vue';
import json from '../../fixtures/file.json';
import Notebook from '../index.vue';

const Component = Vue.extend(Notebook);

describe('Notebook component', () => {
  let vm;

  describe('without JSON', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          notebook: {},
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('does not render', () => {
      expect(vm.$el.tagName).toBeUndefined();
    });
  });

  describe('with JSON', () => {
    beforeEach((done) => {
      vm = new Component({
        propsData: {
          notebook: json,
          codeCssClass: 'js-code-class',
        },
      });
      vm.$mount();

      setTimeout(() => {
        done();
      });
    });

    test('renders cells', () => {
      expect(vm.$el.querySelectorAll('.cell').length).toBe(json.cells.length);
    });

    test('renders markdown cell', () => {
      expect(vm.$el.querySelector('.markdown')).not.toBeNull();
    });

    test('renders code cell', () => {
      expect(vm.$el.querySelector('pre')).not.toBeNull();
    });

    test('add code class to code blocks', () => {
      expect(vm.$el.querySelector('.js-code-class')).not.toBeNull();
    });
  });
});
