import Vue from 'vue';
import json from '../../../fixtures/file.json';
import MarkdownComponent from '../markdown.vue';

const cell = json.cells[1];
const Component = Vue.extend(MarkdownComponent);

describe('Markdown component', () => {
  let vm;

  beforeEach((done) => {
    vm = new Component({
      propsData: {
        cell,
      },
    });
    vm.$mount();

    setTimeout(() => {
      done();
    });
  });

  test('does not render promot', () => {
    expect(vm.$el.querySelector('.prompt span')).toBeNull();
  });

  test('does not render the markdown text', () => {
    expect(
      vm.$el.querySelector('.markdown').innerHTML.trim(),
    ).not.toEqual(cell.source.join(''));
  });

  test('renders the markdown HTML', () => {
    expect(vm.$el.querySelector('.markdown h1')).not.toBeNull();
  });
});
