import Prism from '../highlight';

describe('Highlight library', () => {
  test('imports python language', () => {
    expect(Prism.languages.python).toBeDefined();
  });

  test('uses custom CSS classes', () => {
    const el = document.createElement('div');
    el.innerHTML = Prism.highlight('console.log("a");', Prism.languages.javascript);

    expect(el.querySelector('.s')).not.toBeNull();
    expect(el.querySelector('.nf')).not.toBeNull();
  });
});
