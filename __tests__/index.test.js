import Vue from 'vue';
import NotebookLab from '../index';

Vue.use(NotebookLab);

describe('install function', () => {
  test('creates global component', () => {
    expect(
      Vue.options.components['notebook-lab'],
    ).not.toBeNull();
  });
});
